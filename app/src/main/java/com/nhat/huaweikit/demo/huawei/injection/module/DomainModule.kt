package com.nhat.huaweikit.demo.huawei.injection.module

import dagger.Module

/**
 * Module that provides all dependencies from the domain package/layer.
 */
@Module
abstract class DomainModule